import type { User } from "./User"
import type { Member } from "./Member"
import type { ReceiptItem } from "./ReceiptItem"


type Receipt = {
    id: number
    createdDate: Date
    total: number
    totalBefore: number
    memberDiscount: number
    receivedAmount: number
    change: number
    paymentType: string
    userId: number
    user?: User
    memberId: number
    member?: Member
    receiptItems?: ReceiptItem[]
}

export type { Receipt }
  